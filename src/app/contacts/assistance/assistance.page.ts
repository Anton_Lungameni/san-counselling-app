import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assistance',
  templateUrl: './assistance.page.html',
  styleUrls: ['./assistance.page.scss'],
})
export class AssistancePage implements OnInit {

  public orgs = [
    {
      title: "//Ana-Djeh San Trust",
      description: "<p>Established to promote increased access to secondary and tertiary (college/university) education for San students in Namibia. They do skills training and empowerment workshops with other stakeholders for and with San students based in tertiary institutions in Windhoek. They also do outreach to schools in San communities to motivate and encourage San learner to complete their education for a brighter future.</p>"
      + "<p>The trust aims to promote:</p>"
       + "<ul>"
       + "<li>The San people's rights to education;</li>"
       + "<li>Minimise San pupils' drop-out rates;</li>"
       + "<li>Provide assistance with related activities of education;</li>"
       + "<li>Encourage the necessity of San parents' involvement and support in their children's education; Reduce pregnancy rates among school-going San pupils;</li>"
       + "<li>Encourage self-fulfilment and recognition of San people of their inherent dignity and pride to encourage the development of self-esteem.</li>"
       + "</ul>",
      contact: "<p><strong>Contacts:</strong></p>"
       + "<p>Kileni Fernando</p>"
       + "<p><a href='tel: 0813688808'>0813688808</a></p>"
       + "<p>Email: <a href='mailto:fkileni@yahoo.com'>fkileni@yahoo.com</a></p>"
    },

    {
      title: "Division San Development ",
      description: "<p><em>Office of the Prime Minister</em></p>"
      + "<p>The Division San Development is a special programme under the auspices of the Office of the Prime Minister as mandated by Cabinet to ensure that the marginalised people in Namibia are fully integrated in the mainstream economy of our society</p>",
      contact: "<p><strong>Contacts:</strong></p>"
      + "<p>P/Bag: 13338, Windhoek</p>"
      + "<p><strong>Prime Minister</strong></p>"
      + "<p>Telephone No: <a href='tel:0612879111'> 061 287 9111</a><br /> Telephone No: <a href='tel:0612872002'>061 287 2002</a></p>"
    }, 
    {
      title: "Namibia San Council ",
      description: "The main objectives and goals of the NSC are to advance the social, economic and political development of all San communities. ",
      contact: "<p><strong>Contacts:</strong></p>"
      + "<p>Kileni Fernando</p>"
      + "<p><a href='tel: 0813688808'>0813688808</a></p>"
      + "<p>Email: <a href='mailto:fkileni@yahoo.com'>fkileni@yahoo.com</a></p>"
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
