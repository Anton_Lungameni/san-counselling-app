import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {


  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  youth = new BehaviorSubject([]);
  stories = new BehaviorSubject([]);
  topics = new BehaviorSubject([]);
  types = new BehaviorSubject([]);

  constructor(private plt: Platform,
    private sqlitePorter: SQLitePorter,
    private sqlite: SQLite,
    private http: HttpClient) {
    this.plt.ready().then(() => {
      this.sqlite.create({
        name: 'san6.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          this.seedDatabase();
        });
    });
  }

  seedDatabase() {
    this.http.get('assets/seed.sql', { responseType: 'text' })
      .subscribe(sql => {
        this.sqlitePorter.importSqlToDb(this.database, sql)
          .then(_ => {
            this.loadYouth();
            this.loadTypes();
            this.loadTopics();
            this.loadStories();
            this.dbReady.next(true);
          })
          .catch(e => console.error(e));
      });
  }
  loadStories() {
    const query = 'SELECT * FROM stories';
    return this.database.executeSql(query, []).then(data => {
      const stories = [];
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
          stories.push({
            title: data.rows.item(i).name,
            body: data.rows.item(i).body,
            motivation: data.rows.item(i).motivation,
            id: data.rows.item(i).story_id,
            story_topic: data.rows.item(i).story_topic,
            story_type: data.rows.item(i).story_type,
            youth: data.rows.item(i).youth,
          });
        }
      }
      this.stories.next(stories);
    });
  }

  getStoriesTypes(id) {
    const query = 'SELECT * FROM stories JOIN youth ON youth.youth_id = stories.youth JOIN story_topic ON story_topic.topic_id = stories.story_topic WHERE stories.story_type = ?';
    return this.database.executeSql(query, [id]).then(data => {
      const stories = [];
      //console.log("Here");
      //console.log(data.rows);
      if (data.rows.length > 0) {
        
        for (let i = 0; i < data.rows.length; i++) {
          stories.push({
            title: data.rows.item(i).title,
            topic: data.rows.item(i).topic_title,
            body: data.rows.item(i).body,
            motivation: data.rows.item(i).motivation,
            id: data.rows.item(i).story_id,
            story_topic: data.rows.item(i).story_topic,
            story_type: data.rows.item(i).story_type,
            youth: data.rows.item(i).youth,
            youth_name: data.rows.item(i).name,
            youth_img: data.rows.item(i).img,
            youth_org: data.rows.item(i).organization,
          });
        }
      }
      return stories;
    });
  }
  loadTopics() {
    const query = 'SELECT * FROM story_topic';
    return this.database.executeSql(query, []).then(data => {
      const topics = [];
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
          topics.push({
            title: data.rows.item(i).topic_title,
            id: data.rows.item(i).topic_id,
            img: data.rows.item(i).img,
            story_type: data.rows.item(i).story_type
          });
        }
      }
      this.topics.next(topics);
    });
  }
  loadTypes() {
    const query = 'SELECT * FROM story_type';
    return this.database.executeSql(query, []).then(data => {
      const types = [];
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
          types.push({
            title: data.rows.item(i).title,
            id: data.rows.item(i).type_id,
            img: data.rows.item(i).img,
          });
        }
      }
      this.types.next(types);
    });
  }
  loadYouth() {
    const query = 'SELECT * FROM youth';
    return this.database.executeSql(query, []).then(data => {
      const youth = [];
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
          youth.push({
            name: data.rows.item(i).name,
            id: data.rows.item(i).youth_id,
            organization: data.rows.item(i).organization,
            img: data.rows.item(i).img,
          });
        }
      }
      this.youth.next(youth);
    });
  }

  getDatabaseState() {
    return this.dbReady.asObservable();
  }

  getYouths(): Observable<any[]> {
    return this.youth.asObservable();
  }

  getTypes(): Observable<any[]> {
    return this.types.asObservable();
  }

  getTopics(): Observable<any[]> {
    return this.topics.asObservable();
  }

  getStories(): Observable<any[]> {
    return this.stories.asObservable();
  }

  getYouth(id): Promise<any> {

    return this.database.executeSql('SELECT * FROM youth WHERE youth_id = ?', [id]).then(data => {

      
      return {
        id: data.rows.item(0).youth_id,
        name: data.rows.item(0).name,
        organization: data.rows.item(0).organization,
        img: data.rows.item(0).img
      };
    });
  }

  getType(id): Promise<any> {
    return this.database.executeSql('SELECT * FROM story_type WHERE type_id = ?', [id]).then(data => {
      return {
        id: data.rows.item(0).type_id,
        title: data.rows.item(0).title,
        img: data.rows.item(0).img
      };
    });
  }

  getTopic(id): Promise<any> {
    return this.database.executeSql('SELECT * FROM story_topic WHERE topic_id = ?', [id]).then(data => {
      return {
        id: data.rows.item(0).topic_id,
        title: data.rows.item(0).topic_title,
        img: data.rows.item(0).img,
        story_type: data.rows.item(0).story_type
      };
    });
  }

  // getTypeTopic(id): Promise<any> {
  //   return this.database.executeSql('SELECT * FROM story_topic WHERE story_type = ?', [id]).then(data => {
  //     return {
  //       id: data.rows.item(0).id,
  //       title: data.rows.item(0).title,
  //       img: data.rows.item(0).img,
  //       story_type: data.rows.item(0).story_type
  //     };
  //   });
  // }

  // getTypeStories(id): Promise<any> {
  //   return this.database.executeSql('SELECT * FROM stories WHERE story_type = ?', [id]).then(data => {
  //     return {
  //       id: data.rows.item(0).id,
  //       title: data.rows.item(0).title,
  //       body: data.rows.item(0).body,
  //       motivation: data.rows.item(0).motivation,
  //       youth: data.rows.item(0).youth,
  //       story_topic: data.rows.item(0).story_topic,
  //       story_type: data.rows.item(0).story_type
  //     };
  //   });
  // }

  getStory(id): Promise<any> {
    return this.database.executeSql('SELECT * FROM stories WHERE story_type = ?', [id]).then(data => {
      return {
        id: data.rows.item(0).story_id,
        title: data.rows.item(0).title,
        body: data.rows.item(0).body,
        motivation: data.rows.item(0).motivation,
        youth: data.rows.item(0).youth,
        story_topic: data.rows.item(0).story_topic,
        story_type: data.rows.item(0).story_type
      };
    });
  }

  searchStory(term): Promise<any> {
    console.log(term);
    const query = 'SELECT * FROM stories JOIN youth ON youth.youth_id = stories.youth JOIN story_topic ON story_topic.topic_id = stories.story_topic WHERE stories.title LIKE ? OR story_topic.topic_title LIKE ? OR youth.name LIKE ?';
    return this.database.executeSql(query, ['%' + term + '%', '%' + term + '%', '%' + term + '%' ]).then(data => {
      const stories = [];
      console.log("Here");
      console.log(data.rows);
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
          console.log( data.rows.item(i));
          stories.push({
            title: data.rows.item(i).title,
            topic: data.rows.item(i).topic_title,
            body: data.rows.item(i).body,
            motivation: data.rows.item(i).motivation,
            id: data.rows.item(i).story_id,
            story_topic: data.rows.item(i).story_topic,
            story_type: data.rows.item(i).story_type,
            youth: data.rows.item(i).youth,
            youth_name: data.rows.item(i).name,
            youth_img: data.rows.item(i).img,
            youth_org: data.rows.item(i).organization,
          });
        }
      }
      console.log(stories);
      return stories;
    });
  }


}
