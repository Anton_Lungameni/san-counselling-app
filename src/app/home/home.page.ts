import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../services/database.service';
import { NavigationExtras, Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  types: any[] = [];
  searchTerm : string;
  constructor(private db: DatabaseService, private router: Router) {}

  ngOnInit() {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.db.getTypes().subscribe(type => {
          this.types = type;
        });
      }
    });
  }

  openDetails(type) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        storyType: JSON.stringify(type)
      }
    };
    this.router.navigate(['storylist'], navigationExtras);
  }

  search(evt) {

    this.searchTerm = evt.srcElement.value;
   
  }

  submitSearch(){
    console.log(this.searchTerm);
   // const results = [];
    // Fetch Stories
    this.db.searchStory(this.searchTerm).then(results => {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          stories: JSON.stringify(results)
        }
      };
      this.router.navigate(['stories-results'], navigationExtras);
    })
  
  }

}
