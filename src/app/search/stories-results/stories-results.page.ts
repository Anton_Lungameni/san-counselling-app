import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-stories-results',
  templateUrl: './stories-results.page.html',
  styleUrls: ['./stories-results.page.scss'],
})
export class StoriesResultsPage implements OnInit {

  stories: any;
  constructor(private route: ActivatedRoute,
    private router: Router,) {

      this.route.queryParams.subscribe(params => {
        if (params && params.stories) {
          console.log(params.stories);
          this.stories = JSON.parse(params.stories);
        }
      });

     }

  ngOnInit() {
  }

  OpenStory(story) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        story: JSON.stringify(story)
      }
    };
    this.router.navigate(['storydetails'], navigationExtras);
  }

}
