import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoriesResultsPage } from './stories-results.page';

describe('StoriesResultsPage', () => {
  let component: StoriesResultsPage;
  let fixture: ComponentFixture<StoriesResultsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoriesResultsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoriesResultsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
