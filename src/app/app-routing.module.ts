import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  { path: 'storylist', loadChildren: './storylist/storylist.module#StorylistPageModule' },
  { path: 'storydetails', loadChildren: './storydetails/storydetails.module#StorydetailsPageModule' },
  { path: 'stories-results', loadChildren: './search/stories-results/stories-results.module#StoriesResultsPageModule' },
  { path: 'assistance', loadChildren: './contacts/assistance/assistance.module#AssistancePageModule' },
  { path: 'glossary', loadChildren: './help/glossary/glossary.module#GlossaryPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

