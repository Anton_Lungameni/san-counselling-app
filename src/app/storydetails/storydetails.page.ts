import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-storydetails',
  templateUrl: './storydetails.page.html',
  styleUrls: ['./storydetails.page.scss'],
})
export class StorydetailsPage implements OnInit {
  story: any;
  constructor(private route: ActivatedRoute,
    private router: Router) {
      this.route.queryParams.subscribe(params => {
        if (params && params.story) {
          this.story = JSON.parse(params.story);
        }
      });
     }

  ngOnInit() {
  }

}
