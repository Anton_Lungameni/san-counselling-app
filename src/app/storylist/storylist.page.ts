import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { DatabaseService } from '../services/database.service';

@Component({
  selector: 'app-storylist',
  templateUrl: './storylist.page.html',
  styleUrls: ['./storylist.page.scss'],
})
export class StorylistPage implements OnInit {

  stories: any;
  type: any;
  constructor(private route: ActivatedRoute,
    private router: Router, private db: DatabaseService) {
    this.route.queryParams.subscribe(params => {
      if (params && params.storyType) {
       
       
          this.type = JSON.parse(params.storyType);
        
          this.db.getStoriesTypes(JSON.parse(params.storyType).id).then(data => {
           
            this.stories = data;
       
          });
   
      }
    });
  }

  ngOnInit() {

  }

  OpenStory(story) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        story: JSON.stringify(story)
      }
    };
    this.router.navigate(['storydetails'], navigationExtras);
  }

}
