import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorylistPage } from './storylist.page';

describe('StorylistPage', () => {
  let component: StorylistPage;
  let fixture: ComponentFixture<StorylistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorylistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorylistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
